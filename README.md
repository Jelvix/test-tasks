# Startup Planning API

## Getting Started

Install dependencies via npm (including `devDependencies`):

```bash
npm install
```

Compile project:

```
npx tsc
```

Run it:

```
npm start
```

REST API should be available at `http://localhost:4000` URL.

## API

### GET /api/planning/phases

Returns all current phases with tasks included.

### POST /api/planning/phases

Creates new phase with empty task list.

Body:

```json
{
    "parent_id": "string",
    "name": "string"
}
```

Returns newly created phase.

## POST /api/planning/phases/:phase_id/tasks

Adds a new task to phase with given `phase_id`.

Body:

```json
{
    "title": "string"
}
```

Return newly added task.

## POST /api/planning/phases/:phase_id/tasks/:task_id/status

Attempts to change task status in given phase.

Body:

```json
{
    "is_completed": "boolean"
}
```

Returns both `phase` and `task` objects that were involed in the action.

## Spent time: 124 minutes + 10 minutes of writing this README