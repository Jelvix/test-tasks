export enum AppErrorCode {
    InvalidEntityData = 1,
    OperationNotPermitted = 2,
    NotFound = 3
}

export interface IRawAppError {
    code: number;
    message: string;
}

export class BaseAppError extends Error {
  private readonly code: AppErrorCode;

  constructor(code: AppErrorCode, message: string) {
    super(message);

    this.code = code;

    Object.setPrototypeOf(this, BaseAppError.prototype);
  }

  public getErrorCode(): AppErrorCode {
    return this.code;
  }

  //TODO: move this to separate class, seriazliation like this is not responsibility of this class
  public toJSON(): IRawAppError {
      return {
          code: this.code,
          message: this.message
      }
  }
}

export class InvalidEntityDataError extends BaseAppError  {
    constructor(message: string) {
        super(AppErrorCode.InvalidEntityData, message);
    }
}

export class OperationNotPermittedError extends BaseAppError {
  constructor(message: string) {
    super(AppErrorCode.OperationNotPermitted, message);
  }
}

export class EntityNotFoundError extends BaseAppError {
  constructor(message: string) {
    super(AppErrorCode.NotFound, message);
  }
}


