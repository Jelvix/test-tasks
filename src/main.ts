import fastify from 'fastify'

import PlanningRoutes from './planning/infra/planning.routes'

async function launch() {
    const app = fastify({
        logger: true,
        ignoreTrailingSlash: true
    });

    //TODO: setup custom error handler

    app.register(PlanningRoutes, {
        prefix: '/api/planning'
    });

    try {
        await app.listen(4000);

        console.log(`Started server`)
    } catch(err) {
        console.error(err);
        process.exit(1);
    }
}

launch();