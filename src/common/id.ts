import crypto from 'crypto'

export class Id {
    private readonly uuid: string;

    constructor(value?: string | Id) {
        if (value) {
            if (typeof value === 'string') {
                this.uuid = value;
            } else {
                this.uuid = value.toString();
            }
        } else {
            this.uuid = crypto.randomUUID();
        }
 
        //TODO: ensure provided 'value' is a valid UUID string
    }

    equals(otherId: string | Id) {
        const stringValue = typeof otherId === 'string' ? otherId : otherId.toString();

        return this.uuid === stringValue;
    }

    toString() : string {
        return this.uuid;
    }
}