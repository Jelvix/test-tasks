import { FastifyReply, FastifyRequest, RouteOptions } from "fastify";
import { IPhasesRepository } from "../../app/phases.repository";
import { mapPhaseForApiResponse } from "../phase.mapper";

export default function (phasesRepository: IPhasesRepository) : RouteOptions {
    return {
      method: "GET",
      url: "/phases",
      handler: async (request: FastifyRequest, reply: FastifyReply) => {
        const all = await phasesRepository.findAll();

        return all.map((phase) => mapPhaseForApiResponse(phase));
      },
    };
}   