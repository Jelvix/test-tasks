import { FastifyReply, FastifyRequest, RouteOptions } from "fastify";
import { Id } from "../../../common/id";
import { PlanningAppService } from "../../app/planning.app-service";
import { mapPhaseForApiResponse, mapTaskForApiResponse } from "../phase.mapper";

interface IChangeTaskStatusRequestParams {
  phase_id: string;
  task_id: string;
}

interface IChangeTaskStatusRequestBody {
  is_completed: boolean;
}

export default function (planningAppService: PlanningAppService) : RouteOptions {
  return {
    method: "POST",
    url: "/phases/:phase_id/tasks/:task_id/status",
    schema: {
      body: {
        type: "object",
        properties: {
          is_completed: {
            type: "boolean",
          },
        },
        required: ["is_completed"],
      },
    },
    handler: async (
      request: FastifyRequest,
      reply: FastifyReply
    ) => {
      const params = request.params as IChangeTaskStatusRequestParams;
      const body = request.body as IChangeTaskStatusRequestBody;

      const phaseId = new Id(params.phase_id);
      const taskId = new Id(params.task_id);

      const phase = await planningAppService.changePhaseTaskStatus(
        phaseId,
        taskId,
        body.is_completed
      );

      const task = phase.getTaskById(taskId);

      return {
        phase: mapPhaseForApiResponse(phase),
        task: mapTaskForApiResponse(task!),
      };
    },
  };
}
