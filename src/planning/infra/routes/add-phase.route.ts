import { FastifyReply, FastifyRequest, RouteOptions } from "fastify";
import { FastifyRouteSchemaDef } from "fastify/types/schema";
import { Id } from "../../../common/id";
import { PlanningAppService } from "../../app/planning.app-service";
import { mapPhaseForApiResponse } from "../phase.mapper";

export interface IAddPhaseRequestBody {
  parent_id?: string;
  name: string;
}

export default function (
    planningAppService: PlanningAppService
) : RouteOptions {
    return {
      method: "POST",
      url: "/phases",
      schema: {
        body: {
          type: "object",
          properties: {
            parent_id: {
              type: "string",
              maxLength: 36,
            },
            name: {
              type: "string",
              minLength: 1,
              maxLength: 256,
            },
          },
        },
      },
      handler: async (
        request: FastifyRequest,
        reply: FastifyReply
      ) => {
        const { name, parent_id} = request.body as IAddPhaseRequestBody;

        const createdPhase = await planningAppService.addNewPhase(
          name,
          parent_id ? new Id(parent_id) : undefined
        );

        return mapPhaseForApiResponse(createdPhase);
      },
    };
}