import { FastifyReply, FastifyRequest, RouteOptions } from "fastify";
import { Id } from "../../../common/id";
import { PlanningAppService } from "../../app/planning.app-service";
import { mapTaskForApiResponse } from "../phase.mapper";

interface IAddTaskRequestParams {
  phase_id: string;
}

interface IAddTaskRequestBody {
  title: string;
}

export default function (
    planningAppService: PlanningAppService
) : RouteOptions {
    return {
      method: "POST",
      url: "/phases/:phase_id/tasks",
      schema: {
        body: {
          type: "object",
          properties: {
            title: {
              type: "string",
              minLength: 1,
              maxLength: 512,
            },
          },
          required: ['title']
        },
      },
      handler: async (request: FastifyRequest, reply: FastifyReply) => {
        const params = request.params as IAddTaskRequestParams;
        const phaseId = new Id(params.phase_id);

        const { title } = request.body as IAddTaskRequestBody;

        const task = await planningAppService.addTaskToPhase(phaseId, title);

        return mapTaskForApiResponse(task);
      },
    }
}