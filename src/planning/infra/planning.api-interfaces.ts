export interface IApiTask {
  task_id: string;
  phase_id: string;
  title: string;
  is_completed: boolean;
}

export interface IApiPhase {
  phase_id: string;
  name: string;
  is_phase_complete: boolean;
  parent_phase_id: string | null;
  tasks: IApiTask[];
}
