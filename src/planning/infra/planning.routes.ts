import { FastifyInstance, FastifyPluginOptions } from "fastify";
import { PlanningAppService } from "../app/planning.app-service";
import { InMemoryPhasesRepsitory } from "./inmemory-phases.repository";

import AddPhaseRoute from './routes/add-phase.route'
import GetPhasesRoute from "./routes/get-phases.route";
import AddTaskRoute from "./routes/add-task.route";
import ChangeTaskStatusRoute from "./routes/change-task-status.route";

export default function (
  fastify: FastifyInstance,
  opts: FastifyPluginOptions,
  done: Function
) {
  //TODO: handle instancing of such objects more centrally
  //or even use some IoC container
  const phasesRepository = new InMemoryPhasesRepsitory();
  const planningAppService = new PlanningAppService(phasesRepository);

  fastify.route(AddPhaseRoute(planningAppService));
  fastify.route(GetPhasesRoute(phasesRepository));
  fastify.route(AddTaskRoute(planningAppService));
  fastify.route(ChangeTaskStatusRoute(planningAppService));

  done();
}
