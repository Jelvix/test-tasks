import { Id } from "../../common/id";
import { IPhasesRepository } from "../app/phases.repository";
import { Phase } from "../domain/phase.aggregate";
import { Task } from "../domain/task.entity";

export class InMemoryPhasesRepsitory implements IPhasesRepository {
  private readonly rows: Phase[];

  constructor() {
    this.rows = [];

    this.seedInitialData();
  }

  private seedInitialData() {
    //Some initial seeding with data from task example
    const parent = new Phase({
      name: "First steps",
    });

    parent.addTask(
      new Task({
        title: "Setup virtual office",
        phaseId: parent.getId(),
      })
    );

    parent.addTask(
      new Task({
        title: "Set mission and vision",
        phaseId: parent.getId(),
      })
    );

    parent.addTask(
      new Task({
        title: "Select business name",
        phaseId: parent.getId(),
        isCompleted: true,
      })
    );

    parent.addTask(
      new Task({
        title: "Buy domains",
        phaseId: parent.getId(),
      })
    );

    this.save(parent);

    const discoveryPhase = new Phase({
      parentPhaseId: parent.getId(),
      name: "Discovery",
    });

    discoveryPhase.addTask(
      new Task({
        phaseId: discoveryPhase.getId(),
        title: "Create roadmap",
      })
    );

    discoveryPhase.addTask(
      new Task({
        phaseId: discoveryPhase.getId(),
        title: "Competitor analysis",
      })
    );

    this.save(discoveryPhase);
  }

  public async findById(id: Id): Promise<Phase | null> {
    return this.rows.find((row) => row.getId().equals(id)) || null;
  }

  public async save(phase: Phase): Promise<void> {
    const index = this.rows.findIndex((row) =>
      row.getId().equals(phase.getId())
    );

    if (index === -1) {
      this.rows.push(phase);
    } else {
      this.rows[index] = phase;
    }
  }

  public async findAll(): Promise<Phase[]> {
    return this.rows;
  }

  public async findRootPhase(): Promise<Phase | null> {
    return this.rows.find((row) => !row.hasParentPhase()) || null;
  }

  public async findByParentId(id: Id): Promise<Phase | null> {
    return this.rows.find((row) => row.hasParentPhase() && row.getParentPhaseId()!.equals(id)) || null;
  }
}