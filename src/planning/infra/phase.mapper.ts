import { Phase } from "../domain/phase.aggregate";
import { Task } from "../domain/task.entity";
import { IApiPhase, IApiTask } from "./planning.api-interfaces";


export function mapTaskForApiResponse(task: Task) : IApiTask {
    return {
        task_id: task.getId().toString(),
        phase_id: task.getPhaseId().toString(),
        is_completed: task.isMarkedAsComplete(),
        title: task.getTitle()
    }   
}

export function mapPhaseForApiResponse(phase: Phase): IApiPhase {
    return {
        phase_id: phase.getId().toString(),
        name: phase.getName(),
        is_phase_complete: phase.isCompleted(),
        parent_phase_id: phase.hasParentPhase() ? phase.getParentPhaseId()!.toString() : null,
        tasks: phase.getTasks().map(mapTaskForApiResponse)
    }
}