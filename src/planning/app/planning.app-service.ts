import { Id } from "../../common/id";
import { EntityNotFoundError, InvalidEntityDataError } from "../../errors";
import { Phase } from "../domain/phase.aggregate";
import { PlanningService } from "../domain/planning.service";
import { Task } from "../domain/task.entity";
import { IPhasesRepository } from "./phases.repository";

export class PlanningAppService {
  constructor(private readonly phasesRepository: IPhasesRepository) {}

  public async addNewPhase(name: string, parentId?: Id) {
    if (!parentId) {
      const possibleRootPhase = await this.phasesRepository.findRootPhase();

      if (possibleRootPhase) throw new InvalidEntityDataError(`Root phase is already present, provide a valid parent phase ID`);
    } else {
      const possibleChildPhase = await this.phasesRepository.findByParentId(parentId);

      if (possibleChildPhase) throw new InvalidEntityDataError(`Parent phase ${parentId.toString()} already has one child phase`);
    }

    const phase = new Phase({
      parentPhaseId: parentId!,
      name: name
    });

    await this.phasesRepository.save(phase);

    return phase;
  }

  public async addTaskToPhase(phaseId: Id, title: string): Promise<Task> {
    const phase = await this.phasesRepository.findById(phaseId);

    if (!phase)
      throw new EntityNotFoundError(
        `Phase with ID ${phaseId.toString()} is not found`
      );

    const service = new PlanningService();

    const parent = phase.hasParentPhase()
      ? await this.phasesRepository.findById(phase.getParentPhaseId() as Id)
      : null;

    const task = new Task({
      title,
      phaseId: phase.getId(),
    });

    service.addTaskToPhase(phase, parent, task);

    await this.phasesRepository.save(phase);

    return task;
  }

  public async changePhaseTaskStatus(
    phaseId: Id,
    taskId: Id,
    newStatus: boolean
  ): Promise<Phase> {
    const phase = await this.phasesRepository.findById(phaseId);

    if (!phase)
      throw new EntityNotFoundError(
        `Phase with ID ${phaseId.toString()} is not found`
      );

    const service = new PlanningService();

    const parent = phase.hasParentPhase()
      ? await this.phasesRepository.findById(phase.getParentPhaseId() as Id)
      : null;

    service.changeTaskStatus(phase, parent, taskId, newStatus);

    await this.phasesRepository.save(phase);

    return phase;
  }
}
