import { Id } from "../../common/id";
import { Phase } from "../domain/phase.aggregate";

export interface IPhasesRepository {
    findById(id: Id) : Promise<Phase | null>;
    findRootPhase(): Promise<Phase | null>;
    findByParentId(id: Id): Promise<Phase | null>;
    save(phase: Phase) : Promise<void>;
    findAll(): Promise<Phase[]>;
}