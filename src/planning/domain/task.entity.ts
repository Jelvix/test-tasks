import { Id } from "../../common/id";
import { InvalidEntityDataError, OperationNotPermittedError } from "../../errors";

export interface ITaskConstructionProps {
    id?: Id | string;
    phaseId: Id | string;
    title: string;
    isCompleted?: boolean;
}

export class Task {
    private readonly taskId: Id;
    private phaseId: Id;
    private title: string;
    private isCompleted: boolean;

    constructor(props: ITaskConstructionProps) {
        this.taskId = new Id(props.id);
        this.title = props.title;
        this.phaseId = new Id(props.phaseId);
        this.isCompleted = props.isCompleted || false;

        if (!this.phaseId) {
            throw new InvalidEntityDataError(
              "Task must have a phase ID to which it is assigned"
            );
        }
    }

    public getTitle() : string {
        return this.title;
    }

    public changeTitle(newTitle: string) {
        if (newTitle.length < 1) throw new OperationNotPermittedError(
          `New task title must be at least 1 character long`
        );

        this.title = newTitle;
    }

    public getId() : Id {
        return this.taskId;
    }

    public getPhaseId() : Id {
        return this.phaseId;
    }

    public isMarkedAsComplete() : boolean {
        return this.isCompleted;
    }

    public changeCompletionStatus(newValue: boolean)  {
        this.isCompleted = newValue;
    }
}