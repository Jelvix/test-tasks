import { Id } from "../../common/id";
import {
  InvalidEntityDataError,
  OperationNotPermittedError,
} from "../../errors";
import { Phase } from "./phase.aggregate";
import { Task } from "./task.entity";

export class PlanningService {
  constructor() {}

  public addTaskToPhase(phase: Phase, parentPhase: Phase | null, task: Task) : Task {
    if (phase.hasParentPhase()) {
      if (!parentPhase)
        throw new OperationNotPermittedError(`Parent phase must be present`);
    }

    phase.addTask(task);

    return task;
  }

  public changeTaskStatus(
    phase: Phase,
    parentPhase: Phase | null,
    taskId: Id,
    newStatus: boolean
  ) : Task {
    if (phase.hasParentPhase()) {
      if (!parentPhase)
        throw new OperationNotPermittedError(`Parent phase must be present`);

      if (!parentPhase.isCompleted())
        throw new OperationNotPermittedError(
          `Task status cannot be changed until all previous phases are complete`
        );
    }

    phase.changeTaskStatus(taskId, newStatus);

    return phase.getTaskById(taskId)!;
  }
}
