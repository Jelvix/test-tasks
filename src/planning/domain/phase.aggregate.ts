import { Id } from "../../common/id";
import { EntityNotFoundError, InvalidEntityDataError, OperationNotPermittedError } from "../../errors";
import { Task } from "./task.entity";

export interface IPhaseConstructionProps {
    id?: Id | string;
    name: string;
    tasks?: Task[];
    parentPhaseId?: Id;
}

export class Phase {
    private readonly phaseId: Id;
    private readonly parentPhaseId: Id | null;
    private readonly name: string;

    private readonly tasks: Task[];
    
    constructor(props: IPhaseConstructionProps) {
        this.phaseId = new Id(props.id);
        this.name = props.name;
        this.tasks = props.tasks || [];
        this.parentPhaseId = props.parentPhaseId || null;
    }

    public getId() : Id { 
        return this.phaseId;
    }

    public getName() : string {
        return this.name;
    }

    public getTasks() : Task[] {
        return this.tasks;
    }

    public hasParentPhase() : boolean {
        return this.parentPhaseId !== null;
    }

    public getParentPhaseId() : Id | null {
        return this.parentPhaseId;
    }

    public getTaskById(id: Id) : Task | undefined {
        return this.tasks.find(t => t.getId().equals(id));
    }

    //TODO: we can cache this value to avoid iterating whole array with .every()
    public isCompleted() : boolean {
        return this.tasks.length > 0 && this.tasks.every(task => task.isMarkedAsComplete());
    }

    public isLockedForModification() : boolean {
        return this.isCompleted();
    } 

    private ensureModificationIsAllowed() {
        if (this.isLockedForModification()) throw new OperationNotPermittedError(`Phase is locked, any modification is not allowed`);
    }

    public addTask(task: Task) {
        if (!task.getPhaseId().equals(this.phaseId)) {
            throw new InvalidEntityDataError(`Task phase ID must equal to phase ID it's being added to`);
        }

        this.ensureModificationIsAllowed();

        this.tasks.push(task);
    }

    public changeTaskStatus(taskId: Id, newStatus: boolean) {
        this.ensureModificationIsAllowed();

        const task = this.tasks.find(task => task.getId().equals(taskId));

        if (!task) throw new EntityNotFoundError(`Task with ID ${taskId.toString()} is not found in phase ${this.getId().toString()}`);

        task.changeCompletionStatus(newStatus);
    }
}